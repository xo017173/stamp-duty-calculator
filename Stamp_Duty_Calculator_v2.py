#Imports the tkinter module library
from tkinter import *
from tkinter import messagebox

#Initialises the class and functions
class Person:
    def __init__(self):
        
        #Creates the window
        self.window = Tk()
        self.window.title("LBTT Calculator")#Sets the window title
        self.window.geometry('675x450')#Sets the size of the window width x height
        self.window.config(bg = "lightblue")

        #Creates the label, adds customisations and then places it in window using grid()
        self.titleText = Label(self.window, text = "Stamp Duty Calculator")
        self.titleText.config(bg = "pink", font = ("Arial", 35))
        self.titleText.grid(column = 0, row = 0, columnspan = 1, padx = 100)

        self.costLabel = Label(self.window, text = "Enter house cost: £")
        self.costLabel.config(bg = "pink", font = ("Arial", 20))
        self.costLabel.grid(column = 0, row = 1, columnspan = 1, padx = 10, pady = 30, sticky = "w", ipadx = 5)

        #Does same as above but creates an entry box instead of text 
        self.inputBox = Entry(self.window)
        self.inputBox.config(bg = "pink", font = ("Arial", 20), width = 15)
        self.inputBox.grid(column = 0, row = 1, columnspan = 1, padx = 10, pady = 30, sticky = "e")

        self.yesNoLabel = Label(self.window, text = "Only Home? Yes/No")
        self.yesNoLabel.config(bg = "pink", font = ("Arial", 20))
        self.yesNoLabel.grid(column = 0, row = 2, columnspan = 1, padx = 10, pady = 30, sticky = "w")

        self.yesNoBox = Entry(self.window)
        self.yesNoBox.config(bg = "pink", font = ("Arial", 20), width = 15)
        self.yesNoBox.grid(column = 0, row = 2, columnspan = 1, padx = 10, pady = 30, sticky = "e")

        self.yesNoLabel2 = Label(self.window, text = "First Home? Yes/No")
        self.yesNoLabel2.config(bg = "pink", font = ("Arial", 20))
        self.yesNoLabel2.grid(column = 0, row = 3, columnspan = 1, padx = 10, pady = 30, sticky = "w")

        self.yesNoBox2 = Entry(self.window)
        self.yesNoBox2.config(bg = "pink", font = ("Arial", 20), width = 15)
        self.yesNoBox2.grid(column = 0, row = 3, columnspan = 1, padx = 10, pady = 30, sticky = "e")

        self.calculateButton = Button(self.window, text = "Calculate")
        self.calculateButton.config(bg = "pink", font = ("Arial", 20), width = 15, command = self.calcFunc)
        self.calculateButton.grid(column = 0, row = 4, padx = 10, pady = 30)
        
        self.window.mainloop() #This creates the 'event loop'

    def calcFunc(self):
        self.houseCostStr = self.inputBox.get()
        self.houseCost = int(self.houseCostStr)
        self.secondHouse = self.yesNoBox.get()
        self.firstHome = self.yesNoBox2.get()
        taxB1 = 0
        taxB2 = 0
        taxB3 = 0
        totalTax = 0

        if self.firstHome == "Yes":
            if self.houseCost <= 625000 and self.houseCost > 450000:
                totalTax = self.houseCost - 450000
                totalTax *= 0.05
            elif self.houseCost < 450000:
                totalTax = 0
            elif self.houseCost > 625000:
                if self.houseCost <= 925000 and self.houseCost >= 250001:
                    totalTax = (self.houseCost - 250000) * 0.05
                elif self.houseCost <= 1500000 and self.houseCost >= 925001:
                    taxB1 = 33750
                    taxB2 = (self.houseCost - 925000) * 0.1
                elif self.houseCost > 1500000:
                    taxB1 = 33750
                    taxB2 = 57500
                    taxB3 = (self.houseCost - 1500000) * 0.12

        elif self.secondHouse == "Yes":
            if self.houseCost < 250000:
                totalTax = 0
            else:
                if self.houseCost <= 925000 and self.houseCost >= 250001:
                    totalTax = (self.houseCost - 250000) * 0.05

                elif self.houseCost <= 1500000 and self.houseCost >= 925001:
                    taxB1 = 33750
                    taxB2 = (self.houseCost - 925000) * 0.1
                elif self.houseCost > 1500000:
                    taxB1 = 33750
                    taxB2 = 57500
                    taxB3 = (self.houseCost - 1500000) * 0.12

        elif self.secondHouse == "No":
            if self.houseCost < 250000:
                totalTax = 0
            else:
                if self.houseCost <= 925000 and self.houseCost >= 250001:
                    totalTax = (self.houseCost - 250000) * 0.05

                elif self.houseCost <= 1500000 and self.houseCost >= 925001:
                    taxB1 = 33750
                    taxB2 = (self.houseCost - 925000) * 0.1
                elif self.houseCost > 1500000:
                    taxB1 = 33750
                    taxB2 = 57500
                    taxB3 = (self.houseCost - 1500000) * 0.12
                    totalTax = (taxB1 + taxB2 + taxB3) * 1.03
        else:
            messagebox.showerror(title="Error", message="Enter Valid Data!")
        messagebox.showinfo(title = "Result", message = "Your Stamp Duty Amount is: £" + str(totalTax))
Person()
